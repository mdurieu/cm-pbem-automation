const fs = require('fs');
const exec = require('child_process').execFile;

const args = process.argv;
if (args.length < 4) process.exit();

const combatMissionPath = args[3];
const dropboxPath = args[2];

const files = fs.readdirSync(`${dropboxPath}`);

let mostRecentTime = 0;
let mostRecentFilename;

for (let i = 0; i < files.length; i++) {
  const fileMeta = fs.statSync(`${dropboxPath}\\${files[i]}`);

  const parsedDate = Date.parse(fileMeta.ctime);

  if (parsedDate > mostRecentTime) {
    mostRecentTime = parsedDate;
    mostRecentFilename = files[i];
  }
}

fs.copyFileSync(`${dropboxPath}\\${mostRecentFilename}`, `${combatMissionPath}\\Game Files\\Incoming Email\\${mostRecentFilename}`);

if (args[4]) exec(`${combatMissionPath}\\${args[4]}`);
