
const fs = require('fs');

const args = process.argv;
if (args.length < 4) process.exit();

const combatMissionPath = args[3];
const dropboxPath = args[2];

const files = fs.readdirSync(`${combatMissionPath}\\Game Files\\Outgoing Email`);

let mostRecentTime = 0;
let mostRecentFilename;

for (let i = 0; i < files.length; i++) {
  const fileMeta = fs.statSync(`${combatMissionPath}\\Game Files\\Outgoing Email\\${files[i]}`);

  const parsedDate = Date.parse(fileMeta.ctime);

  if (parsedDate > mostRecentTime) {
    mostRecentTime = parsedDate;
    mostRecentFilename = files[i];
  }
}

fs.copyFileSync(`${combatMissionPath}\\Game Files\\Outgoing Email\\${mostRecentFilename}`, `${dropboxPath}\\${mostRecentFilename}`);
