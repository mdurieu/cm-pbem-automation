# The Cræb’s Combat Mission PBEM Automation Scripts

 This guide should get you through automating upload/download of Play By Email saves using a shared dropbox folder . This was developed using CM Battle for Normandy but I believe it should work on all variations using the same game engine. 

If you are new to how Play By Email works, check out [this](https://www.youtube.com/watch?v=cVMBQrAwsEw) quick video by Usually Hapless.

## 1. Installing Node.js  

The automation scripts are written in Javascript, which requires Node to be installed on your machine to run. Get the installer for your system [here](https://nodejs.org/en/download/). It’s straightforward, just hit all the defaults (and restart your machine after). 

**Verify the installation by opening a command prompt and typing "node --version" (without quotes)** It should output something like "v17.5.0" which is the version this was developed with.

Why Javascript? Because I just threw it together quickly for myself and now here we are. Yes, there are better and more elegant ways to do it.
 
 ## 2. Set up your dropbox folder
 The files that come with these instructions have been set up for a game of Matt vs Allan (With Matt being the local player and Allan the opponent). Swap out the names as appropriate. The scripts can handle multiple simultaneous games.
 
 Create the following folders (with paths modified to where your dropbox folder is located, mine is D:\Dropbox)
 
`D:\Dropbox\Public\CombatMission\MattVsAllan\AllanUploads`

`D:\Dropbox\Public\CombatMission\MattVsAllan\MattUploads`

You will need to share the CombatMission folder with your opponent so they can upload/download into it (I don't think it has to be in Public specifically, but mine is).

You can set up multiple games at once and name them as you please, eg you could also add. 

`D:\Dropbox\Public\CombatMission\SmallSkirmish\AllanUploads`

`D:\Dropbox\Public\CombatMission\SmallSkirmish\MattUploads`

The game name (ie *SmallSkirmish*) does not have to have anything to do with the actual save game file names.

## 3. Copy script files to Combat Mission folder

Copy upload.js and download.js to your Combat Mission folder. They go in the root folder, ie the same folder as the main game .exe (in this case, `CM Normandy.exe`).

## 4. Modify batch files
The scripts are run using batch files which can be run from anywhere on your machine. Mine just sit on the desktop. 

There are 3 batch files included in this repository (again, rename them as suits you) **They need to be modified to point to the correct folders on your machine and for that particular game/opponent**. You'll see these ones are set up for a game Vs Allan, if you are also running a game against Mr Kelly, then you will need to duplicate these batch files and modify them with the details of that game.

The files perform the following functions:


**Allan Up.bat** - Uploads the latest save file from the "Outgoing Emails" folder to the appropriate Dropbox folder. Note that this will copy ONLY the most recent save file. There is only one Outgoing Emails folder in Combat Mission so if you have multiple games going at once, run the batch file for a particular player as soon as you make the save for them.

**Allan Down.bat** - Copies the latest file from `D:\Dropbox\Public\CombatMission\MattVsAllan\AllanUploads` into the Combat Mission `Incoming Emails` folder. Again, there is only one Incoming Emails folder, so if you're running multiple games, you'll have to pick the right file from inside Combat Mission. This can be run at any time, regardless of how many opponents you have.

**Allan Down Launch.bat** - The same function as `Allan Down` but also launches the game. For the extremely lazy.

---

If you've never worked with batch files before, this step might seem intimidating, but all we are doing is changing a few paths. Open up **Allan Up.bat** in notepad. It contains the following:

`node "D:\Games\CMBN\Combat Mission Battle For Normandy\upload.js" D:\Dropbox\Public\CombatMission\MattVsAllan\MattUploads  D:\Games\CMBN\Combat" %~1"Mission" %~1"Battle" %~1"For" %~1"Normandy`


**Note that this is all actually one line, make sure you don't put any new lines in it** Let's break this down into sections of what they do

`node "D:\Games\CMBN\Combat Mission Battle For Normandy\upload.js"` - This tells node to run the upload script file. You will need to **modify this line** to point to your Combat Mission folder where you put upload.js. The quotes are important here as there are spaces in the file path.

It looks ugly, but the rest of the file is 2 parameters that get passed to the upload script detailing where things are located

**Paramater 1** `D:\Dropbox\Public\CombatMission\MattVsAllan\MattUploads` - replace this with the equivalent path to your own Dropbox folder. Note that this path (as taken from my machine) has no spaces in it, if yours has spaces, see Paramater 2 for how to handle them. 

**Paramater 2** `D:\Games\CMBN\Combat" %~1"Mission" %~1"Battle" %~1"For" %~1"Normandy` - This is the path to your Combat Mission folder. Paramaters can't have spaces or be wrapped in quotes, so the very ugly `" %~1"` is inserted in their place (note that there is a space between the first quote and percent sign). **Carefully** edit this so that it points to your Combat Mission folder. If the scripts aren't working, more than likely there's an error in these paths. They need to be exact.

**That's it for the upload file.**

The process is the same for which ever of the two download files you want to use, but will use the `AllanUploads` folder instead of `MattUploads`.

If you are using the Launch batch file, it has an extra parameter of the main game executable. For Battle for Normandy, this file is called `CM Normandy.exe`, which when adapted as a parameter is written as `CM" %~1"Normandy.exe`. This should work if you just replace it with the exe for which ever CM game you're playing.

---

**That's it, you're all done.** Run the upload batch file every time you make a save, and run the download batch file every time your opponent has uploaded their save to Dropbox. These can be run while CM is running, but you might need to exit back to the main menu and into the load game menu to refresh it if you've just downloaded a file.


## N.B.
Also included in this repository is the `HitlerMoods` folder which contains a variety of Hitler reaction images to taunt/communicate with your opponent.







